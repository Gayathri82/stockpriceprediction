**Stock Price Prediction Project**

**1. Dataset Downloading**

Data Source: We have taken the tcs stock price data from kaggle.

Download Link: https://www.kaggle.com/datasets/saikumartamminana/tcs-stock-price-prediction

Data Description: 

This Dataset Contains the Historical Share Price of Tata Consultancy Services ltd(TCS) From 2002 to 2004.

This Dataset Contains seven columns

Date-- Date includes day and month and year

open -- open value of the TCS stock price on particular day

High -- high price value of TCS on particular day

LOW -- Low Price Value Of ITC on particular day

Close --Stock Price of TCS After Closing The Stock Market

Volume -- Volume of TCS means sum of buy's and shares

Adjclose --Adjusted close is the closing price after adjustments for all applicable splits and dividend distributions

**2. EDA (Exploratory Data Analysis)**

Data Cleaning: Our Dataset contains null values in Open, High, Close, Volume columns and we have filled those values using their respective mean values.

Correlation Analysis: We have used different plots to know the correlation between different columns in the dataset and observed that there is high correlation between open and close, High and Close.

**3. Model Building**

our input features include low,high,open,volume and output variable is close.

Training Dataset: 70%

Testing Dataset: 30%

We have used different regression models such as linear regression,random forest regression,decision tree regression on our data.

**4. Testing and Performance Analysis**

Tested each model that we have choosen and linear regression model gave a good Performance of 99.99% r2_score.

**5. Deployment**

Framework: Flask

Frontend: HTML, CSS

Programming Language: Python

