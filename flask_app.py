from flask import Flask, render_template, request
import pickle
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/predict-result', methods=['POST','GET'])
def prediction_result():
    open_price = request.form['open_price']
    high_price = request.form['high_price']
    low_price = request.form['low_price']
    volume = request.form['volume']
    # Perform prediction based on the form data
    prediction = predict_stock_price(open_price, high_price, low_price, volume)
    return render_template('predict-result.html', prediction=prediction)

def predict_stock_price(open_price, high_price, low_price, volume):
    model = pickle.load(open("linear_regression_model.pkl", "rb"))
    return model.predict([[float(open_price), float(high_price), float(low_price), float(volume)]])
if __name__ == '__main__':
    app.run(debug=True)
